package mythreads.pack;

public class SlaveThread implements Runnable {
    private final int interval;
    private static final String namePrefix = "ThreadIntervalIn";
    private Thread thread;
    private boolean suspendFlag;


    public SlaveThread(int interval) throws IntervalValueException {
        if (interval < 1) {
            throw new IntervalValueException();
        }
        this.interval = interval;
        suspendFlag = true;
        thread = new Thread(this, namePrefix + interval);
    }

    public void start(){
        thread.start();
    }

    public void interrupt(){
        thread.interrupt();
    }

    @Override
    public void run() {
        try {
            int localInterval = interval;
            while(!thread.isInterrupted()) {
                synchronized (this) {
                    while (suspendFlag) {
                        wait();
                    }
                }
                localInterval--;
                if (localInterval == 0) {
                    System.out.println("This is a message from " + thread.getName() + " thread");
                    localInterval = interval;
                }
                suspend();
            }
        }catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private synchronized void suspend(){
        suspendFlag = true;
    }

    public synchronized void resume() {
        suspendFlag = false;
        notify();
    }
}
