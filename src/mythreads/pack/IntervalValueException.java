package mythreads.pack;

public class IntervalValueException extends Exception {
    private final static String message = "inteval can not be set lower than 1 second";

    public IntervalValueException() {
        super(message);
    }
}
