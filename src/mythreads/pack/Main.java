package mythreads.pack;
import java.util.ArrayList;

public class Main {

    private static void addNewSlave(ArrayList<SlaveThread> slaveThreads, int interval){
        try {
            SlaveThread newSlave = new SlaveThread(interval);
            slaveThreads.add(newSlave);
        } catch (IntervalValueException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ArrayList<SlaveThread> slaves = new ArrayList<>();
        addNewSlave(slaves, 5);
        addNewSlave(slaves, 7);

        ChronoThread chronoThread = new ChronoThread(slaves);
        chronoThread.start();
        try {
            chronoThread.join();
        }catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
}
