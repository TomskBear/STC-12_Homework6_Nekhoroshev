package mythreads.pack;

import java.util.Collection;

public class ChronoThread extends Thread {

    private static final int interval = 1000;
    private static final int iterations = 10;
    private static final String threadName = "ChronoThread";
    Collection<SlaveThread> slaveThreads;


    public ChronoThread(Collection<SlaveThread> slaveThreads) {
        super(threadName );
        this.slaveThreads = slaveThreads;
    }

    @Override
    public synchronized void start() {
        super.start();
        for (SlaveThread thread : slaveThreads) {
            thread.start();
        }
    }

    public void interruptAll(){
        for (SlaveThread thread : slaveThreads) {
            thread.interrupt();
        }

        super.interrupt();
    }

    @Override
    public void run() {
        int i = 0;
        while(!isInterrupted() && i < iterations) {
            try{
               sleep(interval);
               for (SlaveThread slave : slaveThreads) {
                   slave.resume();
               }
                System.out.println("Chrono thread message");
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            i++;
        }
        interruptAll();
    }
}
